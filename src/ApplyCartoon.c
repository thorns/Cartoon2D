/*@@
  @file      ApplyCartoon.c
  @date      6 Jan 2003
  @author    David Rideout
  @desc 
  Applies the Cartoon boundary condition to all variables 
  selected for a bc.
  @enddesc 
  @version   $Header$
  @@*/

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "util_Table.h"
#include "Cartoon2D.h"
#include "Cartoon2D_tensors.h"
#include "cctk_Groups.h"
#include <stdlib.h>
#include <stdio.h>


static const char *rcsid = "$Header$";

CCTK_FILEVERSION(BetaThorns_Cartoon2D_ApplyCartoon_c);

/********************************************************************
 *********************     Local Data Types   ***********************
 ********************************************************************/
/* #define DEBUG 1 */
#define TENSORTYPE_BUFF_SIZE 7
#define PROLONG_BUFF_SIZE 1000

/********************************************************************
 ********************* Local Routine Prototypes *********************
 ********************************************************************/

/********************************************************************
 ***************** Aliased Routine Prototypes ***********************
 ********************************************************************/

/********************************************************************
 ***************** Scheduled Routine Prototypes *********************
 ********************************************************************/

void Cartoon_ApplyBoundaries(CCTK_ARGUMENTS);

/********************************************************************
 ********************* Other Routine Prototypes *********************
 ********************************************************************/

/********************************************************************
 *********************     Local Data   *****************************
 ********************************************************************/

/********************************************************************
 *********************     Aliased Routines   **********************
 ********************************************************************/
 
/********************************************************************
 *********************     Scheduled Routines   **********************
 ********************************************************************/

/*@@
  @routine    Cartoon_ApplyBoundaries
  @date       6 Jan 2003
  @author     David Rideout
  @desc  
  This will apply the Cartoon boundary condition to all
  variables selected for any (physical) boundary condition.
  @enddesc 
  @calls      
  @history 
  @endhistory 
  @var        CCTK_ARGUMENTS
  @vdesc      Cactus argument list
  @vtype      CCTK_*
  @vio        in
  @endvar
  @returntype void
  @returndesc
  @endreturndesc
  @@*/

void Cartoon_ApplyBoundaries(CCTK_ARGUMENTS) 
{
    DECLARE_CCTK_ARGUMENTS;
    DECLARE_CCTK_PARAMETERS;

    int num_vars, err, i;
    CCTK_INT * vars;

    //return;

    /* Allocate memory to hold selected bcs */
    num_vars = Boundary_SelectedGVs(cctkGH, 0, NULL, NULL, NULL, NULL, NULL);
    vars = malloc(num_vars*sizeof *vars);

    /* get all selected vars */
    err = Boundary_SelectedGVs(cctkGH, num_vars, vars, NULL, NULL, NULL, NULL);
    if (err != num_vars) {
        CCTK_VWarn(1, __LINE__, __FILE__, CCTK_THORNSTRING,
                   "Boundary_SelectedGVs returned %d selected variables, but %d "
                   "expected", err, num_vars);
    }

    /* Apply CartoonBC to each of them. */
    /* One should probably check to see that the entire group has been
     * selected, since Cartoon operates on an entire group at a time.
     * For now I'll just skip a variable if it is not a 'group leader'.
     */
    for (i = 0; i < num_vars; i++) {
        char tensortype[TENSORTYPE_BUFF_SIZE];
        char prolongtype[PROLONG_BUFF_SIZE];
        int group_tags_table, ret;

        int gi = CCTK_GroupIndexFromVarI(vars[i]);
        if (gi < 0) {
            CCTK_VWarn(1, __LINE__, __FILE__, CCTK_THORNSTRING,
                       "Invalid variable index %d selected for a boundary "
                       "condition", gi);
        }
        if (vars[i] != CCTK_FirstVarIndexI(gi)) {
            /* not a group leader -- skip to next var index */
            continue;
        }

        /* Get table handle for group tags table */
        group_tags_table = CCTK_GroupTagsTableI(gi);
        if (group_tags_table < 0) {
            CCTK_VWarn(1, __LINE__, __FILE__, CCTK_THORNSTRING,
                       "Tags table for variable group %s is %d", CCTK_GroupName(gi),
                       group_tags_table);
        }

        /* Get tensor type from group tags table */
        ret = Util_TableGetString(group_tags_table, TENSORTYPE_BUFF_SIZE,
                                  tensortype, "tensortypealias");
        if (ret < 0) {
            CCTK_VWarn(1, __LINE__, __FILE__, CCTK_THORNSTRING,
                       "Error in TAGS table for variable group %s",
                       CCTK_GroupName(gi));
        }

        /* Get prolongation type from group tags table */
        ret = Util_TableGetString(group_tags_table, PROLONG_BUFF_SIZE,
                                  prolongtype, "Prolongation");

        if (!(ret == UTIL_ERROR_TABLE_NO_SUCH_KEY ||
              (ret >= 0 && CCTK_Equals(prolongtype, "Lagrange"))))
            continue;

        /* Call BndCartoon2DVI, passing the appropriate tensor type integer
           macro */
        if (CCTK_Equals(tensortype, "scalar")) {
            BndCartoon2DVI(cctkGH, TENSORTYPE_SCALAR, vars[i]);
        } else if ((CCTK_Equals(tensortype, "u")) || (CCTK_Equals(tensortype, "d"))) {
            BndCartoon2DVI(cctkGH, TENSORTYPE_U, vars[i]);
        } else if (CCTK_Equals(tensortype, "dd_sym")) {
            BndCartoon2DVI(cctkGH, TENSORTYPE_DDSYM, vars[i]);
        } else {
            CCTK_VWarn(0, __LINE__, __FILE__, CCTK_THORNSTRING,
                       "invalid tensor type for group %s", CCTK_GroupName(gi));
        }
    }

    /* Free data */
    free(vars);
}
